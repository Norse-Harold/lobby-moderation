import argparse

from sqlalchemy import create_engine

from moderation.data.database import Base

# These import statements are necessary for successful database
# creation.
import chat_monitor.models
import moderation.data.models


def create_tables(db_url):
    # Create tables in the database.
    Base.metadata.create_all(bind=create_engine(db_url, pool_recycle=3600))


def parse_args():
    """Parse command line arguments.

    Returns:
         Parsed command line arguments

    """
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description="Helper command for database creation")
    parser.add_argument('--database-url', required=True, help='URL for the database to create')
    return parser.parse_args()


def main():
    """Entry point a console script."""
    args = parse_args()
    create_tables(args.database_url)


if __name__ == '__main__':
    main()
