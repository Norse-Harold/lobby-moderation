#!/usr/bin/env python3

# Copyright (C) 2022 Wildfire Games.
# This file is part of 0 A.D.
#
# 0 A.D. is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# 0 A.D. is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with 0 A.D.  If not, see <http://www.gnu.org/licenses/>.

"""Service providing a chatroom language monitor for MUC rooms"""

import argparse
import asyncio
import difflib
import logging
import re
import slixmpp
import sys

from asyncio import Future

from sqlalchemy.orm import scoped_session, sessionmaker

from chat_monitor.models import Blacklist, JIDNickWhitelist, Whitelist, ProfanityIncident
from code import interact
from collections import deque
from datetime import datetime, timedelta
from functools import partial
from moderation.stanzas import ModerationXmppPlugin, ModerationCommand
from pprint import pprint, pformat
from ptpython.repl import embed
from slixmpp.stanza import Iq
from slixmpp.xmlstream import ET
from slixmpp.xmlstream.handler import Callback
from slixmpp.xmlstream.matcher import StanzaPath
from slixmpp.xmlstream.stanzabase import register_stanza_plugin
from sqlalchemy import create_engine, func, or_, not_, select
from sqlalchemy.sql import operators
from traceback import format_exc

class ChatMonitor(slixmpp.ClientXMPP):
    """Main class which monitors chats, issues warnings and mutes if necessary."""

    def __init__(self, sjid, password, rooms, nick, domain, db_url):
        """Initialize the chat monitor."""
        slixmpp.ClientXMPP.__init__(self, slixmpp.jid.JID(sjid), password)
        self.whitespace_keepalive = False

        self.sjid = slixmpp.jid.JID(sjid)
        self.rooms = [slixmpp.jid.JID(room + '@conference.' + domain) for room in rooms]
        self.nick = nick
        self.background_tasks=[]

        engine = create_engine(db_url, pool_recycle=3600)
        self.db_session = scoped_session(
            sessionmaker(autocommit=False, autoflush=False, bind=engine)
        )

        self.shutdown = Future()

        register_stanza_plugin(Iq, ModerationXmppPlugin)
        register_stanza_plugin(ModerationXmppPlugin, ModerationCommand)

        self.add_event_handler('session_start', self._got_session_start)
        for room in self.rooms:
            self.add_event_handler('muc::%s::got_online' % room, self._got_muc_online)
        self.add_event_handler('groupchat_message', self._got_muc_message)
        self.add_event_handler('disconnected', self._got_disconnect)
        self.add_event_handler('failed_all_auth', self._shutdown)

    async def _shutdown(self, event):  # pylint: disable=unused-argument
        """Shut down the bot.

        This is used for aborting connection tries in case the
        configured credentials are wrong, as further connection tries
        won't succeed in this case.

        Arguments:
            event (dict): empty dummy dict

        """
        logging.error("Can't log in. Aborting reconnects.")
        self.abort()
        self.shutdown.set_result(True)

    async def _got_session_start(self, event):  # pylint: disable=unused-argument
        """Join MUC channel and announce presence.

        Arguments:
            event (dict): empty dummy dict

        """
        for room in self.rooms: await self.plugin['xep_0045'].join_muc_wait(room, self.nick)
        self.send_presence()
        self.get_roster()
        logging.info("ChatMonitor started")

    async def _got_disconnect(self, event):  # pylint: disable=unused-argument
        """Reconnect if we're not shutting down.

        Arguments:
            event (dict): empty dummy dict

        """

        logging.info("Got disconnect.")
        logging.info("Reconnecting.")
        # disable_starttls is set here only as a workaround for a bug
        # in Slixmpp and can be removed once that's fixed. See
        # https://lab.louiz.org/poezio/slixmpp/-/merge_requests/226
        # for details.
        self.connect(disable_starttls=None)

    def _got_muc_presence(self, presence):
        """Called for every MUC presence event
        (Currently Unused)

        Arguments:
            presence (slixmpp.stanza.presence.Presence): Received
                presence stanza.
        """
        nick = str(presence['muc']['nick'])
        jid = slixmpp.jid.JID(presence['muc']['jid'])

        logging.debug("Client '%s' connected with a nick of '%s'.", jid, nick)

    def _got_muc_online(self, presence):
        """Called when the first resource for a user has connected to a MUC

        Gets also called whenever a user changes its nick.

        Arguments:
            presence (slixmpp.stanza.presence.Presence): Received
                presence stanza.
        """
        nick = str(presence['muc']['nick'])
        jid = slixmpp.jid.JID(presence['muc']['jid'])
        self._check_matching_nick(jid, nick)

    def _got_muc_offline(self, presence):
        """Called when all resources of a user have disconnected from a muc
        (Currently Unused)

        Arguments:
            presence (slixmpp.stanza.presence.Presence): Received
                presence stanza.

        """
        nick = str(presence['muc']['nick'])
        jid = slixmpp.jid.JID(presence['muc']['jid'])

        logging.debug("Client '%s' with nick '%s' disconnected", jid, nick)

    def _got_muc_message(self, msg):
        """Process messages in the MUC rooms.

        Arguments:
            msg (slixmpp.stanza.message.Message): Received MUC
                message
        """
        if 'stamp' in msg['delay'].xml.attrib: return   # Don't process this message if it is a historical message.
        with self.db_session() as db:
            logging.debug("Received chatroom message")
            nick = msg['muc']['nick']
            room = msg['muc']['room']
            jid = self._get_jid(nick.lower(), room=room)

            if not jid:
                logging.info("Failed to find jid for nick %s", nick)
                return

            if nick == self.nick:
                return
            lowercase_message = msg['body'].lower()

            with db.begin():
                whitelist = db.scalars(select(Whitelist.word)).all()
                blacklist = db.scalars(select(Blacklist.word)).all()

            # Filter out any words on the whitelist.
            filtered_message = lowercase_message
            if whitelist:
                filtered_message = re.sub("|".join(whitelist), "", lowercase_message)

            # If there's a word from the blacklist then add a ProfanityIncident to the database.
            if blacklist and re.search("|".join(blacklist), filtered_message):
                logging.info("(%s Profanity incident) %s: %s", msg['room'],msg['muc']['nick'], msg['body'])
                incident = ProfanityIncident(timestamp=datetime.now(), player=jid.bare, offending_content=lowercase_message)
                db.add(incident)
                db.commit()

                # If there are more than 2 warnings then mute the player.
                number_of_incidents = db.scalar(select(func.count(ProfanityIncident.id)).filter(ProfanityIncident.player==jid.bare, operators.isnot(ProfanityIncident.deleted,True)))
                if number_of_incidents > 2:
                    logging.info("Player has more than 2 incidents. Muting player.")

                    # Mute duration starts at 5 minutes and doubles with each additional mute, but the maximum duration is 1 day.
                    duration = timedelta(minutes=min(1440, 5 * 2 ** max(0, min(number_of_incidents - 2, 9))))
                    logging.info("Mute duration: " + str(duration))

                    iq = self.make_iq_set(ito=slixmpp.jid.JID(self.sjid.bare + "/moderation"))

                    iq.enable('moderation')
                    iq['moderation']['moderation_command']['command_name'] = "mute"
                    iq['moderation']['moderation_command']['params'] = {"jid": jid.bare,
                                                                        "duration": str(duration), "reason": "Profanity",
                                                                        "moderator": "userbot@lobby.wildfiregames.com"}
                    try:
                        iq.send()
                    except: logging.exception(format_exc())
                    
                else: 
                    logging.info("Kicking player from the room with a warning.")
                    to = slixmpp.jid.JID(self.sjid)
                    to.resource="moderation"
                    iq = self.make_iq_set()
                    iq['to']=to
                    iq.enable('moderation')
                    iq['moderation']['moderation_command']['command_name'] = "kick"
                    iq['moderation']['moderation_command']['params'] = {"jid": jid.bare, "reason": "Don't use profanity in the main lobby.",
                                                                        "moderator": "userbot@lobby.wildfiregames.com"}
                    try:
                        iq.send()
                    except: logging.exception(format_exc())

    def _get_jid(self, nick, room=None):
        """Return JID for the nick in a MUC room or all rooms.
            
        Arguments:
            room (slixmpp.jid.JID): (optional) MUC room
            
        """ 
        nick = nick.lower()
        rooms = [ room ] if room else self.rooms
        for room in rooms:
            roster = [nick_.lower() for nick_ in self.plugin['xep_0045'].get_roster(room)]
            if nick in roster: return slixmpp.jid.JID(self.plugin['xep_0045'].get_jid_property(room, nick, "jid"))
        return False
        
    def _get_roster_jids(self, room):
        """Return roster for the MUC room as a dict of JIDs keyed by
        nickname
        
        Arguments:
            room (slixmpp.jid.JID): MUC room
            
        """ 
        roster = self.plugin['xep_0045'].get_roster(room)
        result = {}
        for nick in roster:
            result[nick]=slixmpp.jid.JID(self.plugin['xep_0045'].get_jid_property(room, nick, "jid"))
        return result

    def _check_matching_nick(self, jid, nick):
        """Kick users whose local JID part doesn't match their nick.

        Arguments:
            jid: (slixmpp.jid.JID): JID of the connected user
            nick (str): Nick the user chose for the MUC
        """
        if jid.node.lower() == nick.lower():
            return

        if jid.bare == self.sjid.bare:
            return

        with self.db_session() as db:
            whitelist = db.scalars(select(JIDNickWhitelist.jid)).all()
            if jid.bare in whitelist:
                return

            reason = f"User {jid} connected with a nick different to their JID: {nick}"
            logging.info(reason)

            incident = ProfanityIncident(timestamp=datetime.now(), player=jid.bare,
                                         offending_content=reason)
            db.add(incident)
            db.commit()

        to = slixmpp.jid.JID(self.sjid)
        to.resource = "moderation"
        iq = self.make_iq_set()
        iq['to'] = to
        iq.enable('moderation')
        iq['moderation']['moderation_command']['command_name'] = "kick"
        iq['moderation']['moderation_command']['params'] = {"jid": jid.bare,
                                                            "nick": nick,
                                                            "reason": "Don't try to impersonate "
                                                                      "other users",
                                                            "moderator": self.sjid.bare}
        try:
            iq.send()
        except Exception:
            logging.exception(format_exc())


def parse_args(args):
    """Parse command line arguments.

    Arguments:
        args (dict): Raw command line arguments given to the script

    Returns:
         Parsed command line arguments

    """
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description="XMPP interface for the lobby moderation service")

    log_settings = parser.add_mutually_exclusive_group()
    log_settings.add_argument('-q', '--quiet', help="only log errors", action='store_const',
                              dest='log_level', const=logging.ERROR)
    log_settings.add_argument('-d', '--debug', help="log debug messages", action='store_const',
                              dest='log_level', const=logging.DEBUG)
    log_settings.add_argument('-v', '--verbose', help="log more informative messages",
                              action='store_const', dest='log_level', const=logging.INFO)
    log_settings.set_defaults(log_level=logging.WARNING)

    parser.add_argument('-m', '--domain', help="XMPP server to connect to",
                        default='lobby.wildfiregames.com')
    parser.add_argument('-l', '--login', help="username for login", default='moderation')
    parser.add_argument('-p', '--password', help="password for login", default='password')
    parser.add_argument('-n', '--nickname', help="nickname shown to players", default='ModerationBot')
    parser.add_argument('-r', '--rooms', help="MUC rooms to join", nargs="+", default=['arena27'])
    parser.add_argument('--database-url', help="URL for the moderation database",
                        default='sqlite:///moderation.sqlite3')
    parser.add_argument('-s', '--server', help='address of the XMPP server',
                        action='store', dest='xserver', default=None)
    parser.add_argument('-t', '--disable-tls',
                        help='Pass this argument to connect without TLS encryption',
                        action='store_true', dest='xdisabletls', default=False)

    return parser.parse_args(args)


async def async_main():
    """Entry point a console script."""
    args = parse_args(sys.argv[1:])

    logging.basicConfig(level=args.log_level,
                        format='%(asctime)s %(levelname)-8s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    xmpp = ChatMonitor('%s@%s/%s' % (args.login, args.domain, 'chat_monitor'), args.password,
                   args.rooms, args.nickname, args.domain, args.database_url)
    xmpp.register_plugin('xep_0030')  # Service Discovery
    xmpp.register_plugin('xep_0004')  # Data Forms
    xmpp.register_plugin('xep_0045')  # Multi-User Chat
    xmpp.register_plugin('xep_0060')  # Publish-Subscribe
    xmpp.register_plugin('xep_0199', {'keepalive': True})  # XMPP Ping

    if args.xserver:
        xmpp.connect((args.xserver, 5222), disable_starttls=args.xdisabletls)
    else:
        xmpp.connect(None, disable_starttls=args.xdisabletls)
    
    # Start a debug console
    console = asyncio.get_event_loop().create_task(partial(embed,globals=globals(), locals=locals(), return_asyncio_coroutine=True, patch_stdout=True)())
    try: await console
    except: logging.exception(format_exc())

    await xmpp.shutdown


def main():
    asyncio.run(async_main())

if __name__ == '__main__':
    main()
