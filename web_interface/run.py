from web_interface.web_interface import create_app

if __name__ == '__main__':
    app = create_app()
    app.run()
